# HerickoStore

## Introduction

This project is a simple store backend system for products and orders, 
with functionality that is exposed via a REST API

## How to install

To run the app cd into the directory and execute the command 'mvn spring-boot:run'
if you have maven installed locally, or execute command 'mvnw.cmd spring-boot:run' 
on Windows or 'mvnw spring-boot:run' on Unix. All commands also require a Java 
installation

## Documentation access
The API is documented using swagger and is reachable at {host}/swagger-ui.html#/, eg.
http://localhost:8080/swagger-ui.html#/ when running locally 
