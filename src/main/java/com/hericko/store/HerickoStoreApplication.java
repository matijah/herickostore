package com.hericko.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HerickoStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(HerickoStoreApplication.class, args);
	}
}
