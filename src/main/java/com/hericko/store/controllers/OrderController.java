package com.hericko.store.controllers;

import com.hericko.store.aop.logging.LogRequest;
import com.hericko.store.models.Order;
import com.hericko.store.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/${api.path}/${api.version}/orders")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    @LogRequest
    public ResponseEntity<List<Order>> getAllOrders(@RequestParam(name = "from", required = false, defaultValue = "1970-01-01T01:00:00.000Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from,
                                                    @RequestParam(name = "to", required = false, defaultValue = "2170-01-01T01:00:00.000Z") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date to) {
        List<Order> orders = orderService.getOrdersBetween(from, to);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @PostMapping
    @LogRequest
    public ResponseEntity<Order> placeOrder(@RequestBody() Order order) {
        Order placedOrder = orderService.place(order);
        return new ResponseEntity<>(placedOrder, HttpStatus.CREATED);
    }
}
