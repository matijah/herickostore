package com.hericko.store.controllers;

import com.hericko.store.aop.logging.LogRequest;
import com.hericko.store.models.Product;
import com.hericko.store.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/${api.path}/${api.version}/products")
public class ProductController {

    private ProductService productService;

    @Autowired
    ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    @LogRequest
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping
    @LogRequest
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        Product createdProduct = productService.createProduct(product);
        return new ResponseEntity<>(createdProduct, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @LogRequest
    public ResponseEntity<Product> updateProduct(@PathVariable(value = "id") int id, @RequestBody Product product) {
        Product createdProduct = productService.updateProduct(id, product);
        return new ResponseEntity<>(createdProduct, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @LogRequest
    public ResponseEntity<Product> getProduct(@PathVariable(value = "id") int id) {
        Product product = productService.getProductById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}
