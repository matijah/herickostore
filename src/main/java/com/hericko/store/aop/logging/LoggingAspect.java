package com.hericko.store.aop.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Before("@annotation(com.hericko.store.aop.logging.LogRequest)")
    public void logBefore(JoinPoint joinPoint) {
        logger.info(String.format("Received request to method '%s'", formatFullMethodSignature(joinPoint)));
        logger.debug(String.format("Entering method '%s' with arguments '%s'",
                formatFullMethodSignature(joinPoint),
                Arrays.toString(joinPoint.getArgs()))
        );
    }

    @AfterReturning(pointcut = "@annotation(com.hericko.store.aop.logging.LogRequest)", returning = "result")
    public void logAfter(JoinPoint joinPoint, Object result) {
        logger.info(String.format("Completed request to method '%s'", formatFullMethodSignature(joinPoint)));
        logger.debug(String.format("Method Return value : %s", parseResponseValueFromResult(result)));
    }

    @AfterThrowing(pointcut = "@annotation(com.hericko.store.aop.logging.LogRequest)", throwing = "exception")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable exception) {
        logger.error(String.format("An exception has been thrown in method '%s'", formatFullMethodSignature(joinPoint)));
        logger.error("Message: " + exception.getMessage());
        if (exception.getCause() != null) {
            logger.error("Cause: " + exception.getCause());
        }
    }

    @Around("@annotation(com.hericko.store.aop.logging.LogRequest)")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        try {
            Object result = joinPoint.proceed();

            long elapsedTime = System.currentTimeMillis() - start;

            logger.debug(String.format("Method '%s' took %d ms.", formatFullMethodSignature(joinPoint), elapsedTime));

            return result;
        } catch (IllegalArgumentException e) {
            logger.error(String.format("Illegal argument '%s' in method '%s'",
                    Arrays.toString(joinPoint.getArgs()),
                    formatFullMethodSignature(joinPoint))
            );
            throw e;
        }
    }

    private String formatFullMethodSignature(JoinPoint joinPoint) {
        return joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName() + "()";
    }

    private String parseResponseValueFromResult(Object result) {
        if (result instanceof ResponseEntity) {
            ResponseEntity responseEntity = (ResponseEntity) result;
            return responseEntity.getStatusCode().toString();
        }
        return result.toString();
    }
}