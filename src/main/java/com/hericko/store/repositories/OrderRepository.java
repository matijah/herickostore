package com.hericko.store.repositories;

import com.hericko.store.models.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findByPlacedAtBetween(Date from, Date to);
}
