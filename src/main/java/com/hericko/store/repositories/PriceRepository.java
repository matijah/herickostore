package com.hericko.store.repositories;

import com.hericko.store.models.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface PriceRepository extends JpaRepository<Price, Integer> {

    Price findByProductIdAndValidToIsNull(Integer productId);

    @Query("SELECT p FROM Price p WHERE p.product.id = ?1 AND p.validFrom <= ?2 AND (p.validTo >= ?2 OR p.validTo IS NULL)")
    Price findPriceForProductValidAtDate(Integer id, Date timeOfPrice);
}
