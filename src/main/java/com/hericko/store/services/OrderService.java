package com.hericko.store.services;

import com.hericko.store.models.Order;

import java.util.Date;
import java.util.List;

public interface OrderService {
    Order place(Order order);

    List<Order> getOrdersBetween(Date from, Date to);
}
