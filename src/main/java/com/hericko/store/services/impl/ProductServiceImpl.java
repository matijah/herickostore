package com.hericko.store.services.impl;

import com.hericko.store.models.Product;
import com.hericko.store.repositories.ProductRepository;
import com.hericko.store.services.PriceService;
import com.hericko.store.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private PriceService priceService;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, PriceService priceService) {
        this.productRepository = productRepository;
        this.priceService = priceService;
    }

    @Override
    public Product getProductById(int id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            return product.get();
        }
        throw new IllegalArgumentException("Product with id " + id + " is not present.");
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product createProduct(Product product) {
        validateProduct(product, true);
        Product savedProduct = productRepository.save(product);
        priceService.createPriceForProduct(savedProduct);
        return savedProduct;
    }

    @Override
    public Product updateProduct(int id, Product product) {
        validateProduct(product, false);
        product.setId(id);
        Product updatedProduct = productRepository.save(product);
        priceService.updatePriceForProduct(product);
        return updatedProduct;
    }

    private void validateProduct(Product product, boolean checkIdIsEmpty) {
        if (product == null) {
            throw new IllegalArgumentException("Product should not be null");
        }

        if (checkIdIsEmpty && product.getId() != null) {
            throw new IllegalArgumentException("When creating a new product, field id must not be set");
        }

        if (product.getName() == null || product.getName().isEmpty()) {
            throw new IllegalArgumentException("Product must have a non empty name field");
        }

        if (product.getPrice() == null) {
            throw new IllegalArgumentException("Product must have a price");
        }
    }
}
