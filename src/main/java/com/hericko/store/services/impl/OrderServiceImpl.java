package com.hericko.store.services.impl;

import com.hericko.store.models.Order;
import com.hericko.store.models.Price;
import com.hericko.store.models.Product;
import com.hericko.store.repositories.OrderRepository;
import com.hericko.store.services.OrderService;
import com.hericko.store.services.PriceService;
import com.hericko.store.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private ProductService productService;
    private PriceService priceService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, ProductService productService, PriceService priceService) {
        this.orderRepository = orderRepository;
        this.productService = productService;
        this.priceService = priceService;
    }

    @Override
    public Order place(Order order) {
        validateOrder(order);
        order.setPlacedAt(new Date());
        Order persistedOrder = orderRepository.save(order);
        calculateTotals(Collections.singletonList(persistedOrder));
        return persistedOrder;
    }

    @Override
    public List<Order> getOrdersBetween(Date from, Date to) {
        validateDates(from, to);
        List<Order> orders = orderRepository.findByPlacedAtBetween(from, to);
        calculateTotals(orders);
        return orders;
    }

    private void calculateTotals(List<Order> orders) {
        for (Order order : orders) {
            Date placedAt = order.getPlacedAt();

            BigDecimal orderTotal = new BigDecimal(0);

            for (Product product : order.getProducts()) {
                Price productPrice = priceService.getPriceForProductAtTime(product, placedAt);

                product.setPrice(productPrice.getPrice());
                orderTotal = orderTotal.add(productPrice.getPrice());
            }
            order.setOrderTotal(orderTotal);
        }
    }

    private void validateOrder(Order order) {
        if (order == null ) {
            throw new IllegalArgumentException("Order should not be null");
        }

        if (order.getId() != null) {
            throw new IllegalArgumentException("When placing a new order, field id must not be set");
        }

        if (order.getPlacedAt() != null) {
            throw new IllegalArgumentException("When placing a new order, field placedAt must not be set");
        }

        if (order.getBuyerEmail() == null || order.getBuyerEmail().isEmpty()) {
            throw new IllegalArgumentException("Buyers email must be present on the order");
        }

        if (order.getProducts() == null || order.getProducts().isEmpty()) {
            throw new IllegalArgumentException("An order must contain at least one product");
        }

        List<Product> persistedProducts = order.getProducts().stream()
                .map(Product::getId)
                .map(productService::getProductById)
                .collect(Collectors.toList());

        order.setProducts(persistedProducts);
    }

    private void validateDates(Date from, Date to) {
        if (from.after(to)) {
            throw new IllegalArgumentException("From date should be before to date");
        }
    }
}
