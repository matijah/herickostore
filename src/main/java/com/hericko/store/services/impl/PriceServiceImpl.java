package com.hericko.store.services.impl;

import com.hericko.store.models.Price;
import com.hericko.store.models.Product;
import com.hericko.store.repositories.PriceRepository;
import com.hericko.store.services.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PriceServiceImpl implements PriceService {

    private PriceRepository priceRepository;

    @Autowired
    public PriceServiceImpl(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public void createPriceForProduct(Product product) {
        Price price = new Price(product);
        priceRepository.save(price);
    }

    @Override
    public void updatePriceForProduct(Product product) {
        Date now = new Date();

        Price previousPrice = priceRepository.findByProductIdAndValidToIsNull(product.getId());

        if (previousPrice != null) {
            previousPrice.setValidTo(now);
            priceRepository.save(previousPrice);
        }

        Price newPrice = new Price(product);
        newPrice.setValidFrom(now);
        priceRepository.save(newPrice);
    }

    @Override
    public Price getPriceForProductAtTime(Product product, Date timeOfPrice) {
        return priceRepository.findPriceForProductValidAtDate(product.getId(), timeOfPrice);
    }
}
