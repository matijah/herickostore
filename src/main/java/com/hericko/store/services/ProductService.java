package com.hericko.store.services;

import com.hericko.store.models.Product;

import java.util.List;

public interface ProductService {

    Product getProductById(int id);

    List<Product> getAllProducts();

    Product createProduct(Product product);

    Product updateProduct(int id, Product product);
}
