package com.hericko.store.services;

import com.hericko.store.models.Price;
import com.hericko.store.models.Product;

import java.util.Date;

public interface PriceService {

    void createPriceForProduct(Product product);

    void updatePriceForProduct(Product product);

    Price getPriceForProductAtTime(Product product, Date timeOfPrice);
}
