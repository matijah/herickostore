package com.hericko.store.services.impl;

import com.hericko.store.models.Order;
import com.hericko.store.models.Price;
import com.hericko.store.models.Product;
import com.hericko.store.repositories.OrderRepository;
import com.hericko.store.services.OrderService;
import com.hericko.store.services.PriceService;
import com.hericko.store.services.ProductService;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class OrderServiceImplTest {

    @Test
    public void testPlaceOrder_orderIsNull_validationException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        // Act
        try {
            orderService.place(null);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("Order should not be null", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderIdIsSetWhenCreating_expectException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Order order = new Order();
        order.setId(1);

        // Act
        try {
            orderService.place(order);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("When placing a new order, field id must not be set", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderPlacedAtIsSetWhenCreating_expectException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Order order = new Order();
        order.setPlacedAt(new Date());

        // Act
        try {
            orderService.place(order);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("When placing a new order, field placedAt must not be set", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderBuyerEmailIsNotSetWhenCreating_expectException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Order order = new Order();

        // Act
        try {
            orderService.place(order);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("Buyers email must be present on the order", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderProductsIsNullWhenCreating_expectException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Order order = new Order();
        order.setBuyerEmail("example@example.com");

        // Act
        try {
            orderService.place(order);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("An order must contain at least one product", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderProductsIsEmptyWhenCreating_expectException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Order order = new Order();
        order.setBuyerEmail("example@example.com");
        order.setProducts(Collections.emptyList());

        // Act
        try {
            orderService.place(order);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("An order must contain at least one product", ex.getMessage());
        }
    }

    @Test
    public void testPlaceOrder_orderIsOk_expectCallToRepository() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        Product product = new Product();
        product.setId(1);
        product.setName("Product1");
        product.setPrice(new BigDecimal(10));
        when(productService.getProductById(1)).thenReturn(product);

        Order order = new Order();
        order.setBuyerEmail("example@example.com");
        order.setProducts(Collections.singletonList(product));
        when(orderRepository.save(order)).thenReturn(order);

        Price price = new Price();
        price.setId(1);
        price.setProduct(product);
        price.setPrice(new BigDecimal(20));
        price.setValidFrom(new Date(0));
        when(priceService.getPriceForProductAtTime(any(), any())).thenReturn(price);

        // Act
        orderService.place(order);

        // Assert
        verify(orderRepository, times(1)).save(order);
    }

    @Test
    public void testGetOrdersBetween_fromIsAfterTo_validationException() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        // Act
        try {
            orderService.getOrdersBetween(new Date(), new Date(0));
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("From date should be before to date", ex.getMessage());
        }
    }

    @Test
    public void testGetOrdersBetween_delegatesToRepository() {
        // Arrange
        OrderRepository orderRepository = mock(OrderRepository.class);
        ProductService productService = mock(ProductService.class);
        PriceService priceService = mock(PriceService.class);

        OrderService orderService = new OrderServiceImpl(orderRepository, productService, priceService);

        // Act
        orderService.getOrdersBetween(new Date(0), new Date());

        // Assert
        verify(orderRepository, times(1)).findByPlacedAtBetween(any(), any());
    }
}