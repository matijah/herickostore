package com.hericko.store.services.impl;

import com.hericko.store.models.Product;
import com.hericko.store.repositories.ProductRepository;
import com.hericko.store.services.PriceService;
import com.hericko.store.services.ProductService;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {

    @Test
    public void testGetProductById_productIsNotExisting_expectException() {
        // Arrange
        int id = 1;
        ProductRepository productRepository = mock(ProductRepository.class);
        PriceService priceService = mock(PriceService.class);
        ProductService productService = new ProductServiceImpl(productRepository, priceService);

        when(productRepository.findById(id)).thenReturn(Optional.empty());
        // Act
        try {
            productService.getProductById(id);
        } catch (IllegalArgumentException ex) {
            // Assert
            assertEquals("Product with id " + id + " is not present.", ex.getMessage());
        }
    }

    @Test
    public void testGetProductById_productIsExisting_expectProduct() {
        // Arrange
        ProductRepository productRepository = mock(ProductRepository.class);
        PriceService priceService = mock(PriceService.class);
        ProductService productService = new ProductServiceImpl(productRepository, priceService);

        Product product = new Product();
        product.setId(1);
        product.setName("Product");
        product.setPrice(new BigDecimal(10));

        when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        // Act
        Product result = productService.getProductById(product.getId());

        // Assert
        assertEquals(product, result);
    }

}