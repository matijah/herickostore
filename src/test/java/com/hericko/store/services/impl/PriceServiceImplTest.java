package com.hericko.store.services.impl;

import com.hericko.store.models.Product;
import com.hericko.store.repositories.PriceRepository;
import com.hericko.store.services.PriceService;
import org.junit.Test;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PriceServiceImplTest {

    @Test
    public void testCreatePriceForProduct_delegatesToRepository() {
        // Arrange
        PriceRepository priceRepository = mock(PriceRepository.class);
        PriceService priceService = new PriceServiceImpl(priceRepository);

        Product product = new Product();
        // Act
        priceService.createPriceForProduct(product);

        // Assert
        verify(priceRepository, times(1)).save(any());
    }

    @Test
    public void testGetPriceForProductAtTime_delegatesToRepository() {
        // Arrange
        PriceRepository priceRepository = mock(PriceRepository.class);
        PriceService priceService = new PriceServiceImpl(priceRepository);

        Product product = new Product();
        // Act
        priceService.getPriceForProductAtTime(product, new Date());

        // Assert
        verify(priceRepository, times(1)).findPriceForProductValidAtDate(any(), any());
    }
}